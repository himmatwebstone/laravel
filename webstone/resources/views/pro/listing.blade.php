@if(Session::has('email'))

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2 style="text-align: center;">User's Data</h2>
<a href="/" class="btn btn-primary">Add More</a>
<table>
  <tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Username</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Action</th>
    <th>Action</th>
  </tr>
      @foreach($users as $user)

      <tr>       
        <td>{{$user->first_name}}</td>
        <td>{{$user->last_name}}</td>
        <td>{{$user->username}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->phone_number}}</td>
        <td><a href="delete/{{ $user->id }}">delete</a></td>
        <td><a href="update_listing/{{ $user->id }}">Edit</a></td>
      </tr>

    @endforeach
</table>

</body>
</html>
@else
<script>window.location = "/login";</script>
@endif