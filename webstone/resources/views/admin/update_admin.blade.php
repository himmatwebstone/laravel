@extends('admin.master');

@section('content');

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>General Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="submit" method="post">
                @csrf
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">First Name:</label>
                    <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name">
                  </div>
                  <span style="color: red">@error('first_name'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Last Name:</label>
                    <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" name="last_name">
                  </div>
                  <span style="color: red">@error('last_name'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Username:</label>
                   <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username">
                  </div>
                  <span style="color: red">@error('username'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Email:</label>
                    <input type="text" class="form-control" id="email" placeholder="Enter email" name="email">
                  </div>
                  <span style="color: red">@error('email'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Phone Number:</label>
                    <input type="text" class="form-control" id="phone_number" placeholder="Enter Phone Number" name="phone_number">
                  </div>
                  <span style="color: red">@error('phone_number'){{$message}}@enderror</span>


                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                   <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
                  </div>
                  <span style="color: red">@error('password'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password:</label>
                    <input type="password" class="form-control" id="password" placeholder="Enter Confirm Password" name="confirm_password">
                  </div>
                  <span style="color: red">@error('confirm_password'){{$message}}@enderror</span>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">

                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                  <button type="submit" class="btn btn-primary">Submit</button>

                </div>

              </form>
            </div>

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection