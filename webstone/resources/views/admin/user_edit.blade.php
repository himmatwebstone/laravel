@extends('admin.master');

@section('content');
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User's Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User's Data</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             <form action="{{route('submit_update')}}" method="post">
               
                  <div class="form-group">
                      
                    <label for="first_name">First Name:</label>
                    <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" value="{{$user->first_name}}">
                  </div>
                  <span style="color: red">@error('first_name'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="last_name">Last Name:</label>
                    
                    <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" name="last_name" value="{{$user->last_name}}">
                  </div>
                  <span style="color: red">@error('last_name'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="username">Username:</label>
                    
                    <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" value="{{$user->username}}">
                  </div>
                  <span style="color: red">@error('username'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="email">Email:</label>
                    
                    <input type="text" class="form-control" id="email" placeholder="Enter email" name="email" value="{{$user->email}}" disabled>
                  </div>
                  <span style="color: red">@error('email'){{$message}}@enderror</span>

                  <div class="form-group">
                    <label for="phone_number">Phone Number:</label>
                    
                    <input type="text" class="form-control" id="phone_number" placeholder="Enter Phone Number" name="phone_number" value="{{$user->phone_number}}">
                  </div>

                  <span style="color: red">@error('phone_number'){{$message}}@enderror</span>

                
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                  <input type="hidden" class="form-control" id="" placeholder="" name="id" value="{{$user->id}}">

                  <button type="submit" class="btn btn-primary">Update</button>
                  <a href="profile" class="btn btn-danger">Profile</a>
                  
                </form>

            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
