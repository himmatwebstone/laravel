

@extends('admin.master');

@section('content');

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User's Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User's Data</li>
              <li><a href="logout" class="btn btn-primary">Logout</button></a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
              </div>
              @if(Session::has('success'))

                  <div class="alert alert-dangers">

                    <span style="color: #084014;">{{Session::get('success')}}</span>   

                  </div>

              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Action</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  	@foreach($users as $user)
                 	<tr>
	                  	<td>{{$user->first_name}}</td>
				        <td>{{$user->last_name}}</td>
				        <td>{{$user->username}}</td>
				        <td>{{$user->email}}</td>
				        <td>{{$user->phone_number}}</td>
				        <td><a href="delete/{{ $user->id }}">delete</a></td>
				        <td><a href="user_edit/{{ $user->id }}">Edit</a></td>
                  	</tr>
                   @endforeach
                  <tr>
                    
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection