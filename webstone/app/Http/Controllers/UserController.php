<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function index() {

        return view('pro/index');

    }


    public function submit(Request $request) {

        $request->validate(['first_name'       => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'last_name'        => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'username'         => 'required | max:10|regex:/^[\pL\s\-]+$/u',
                            'email'            => 'required | email',
                            'phone_number'     => 'required | max:10',
                            'password'         => 'required | min:6',
                            'confirm_password' => 'required']);


        $first_name       = $request->input('first_name');
        $last_name        = $request->input('last_name');
        $username         = $request->input('username');
        $email            = $request->input('email');
        $phone_number     = $request->input('phone_number');
        $password         = $request->input('password');
        $confirm_password = $request->input('confirm_password');
       

        if ($password == $confirm_password){
            $password = md5($password);
            
            DB::insert("insert into  user(first_name, last_name, username, email, phone_number, password, role) values('$first_name','$last_name','$username','$email','$phone_number','$password', '1')");

            return back()->with('success','User Add successfully!');

            return redirect('user_listing');

        }else{

            return back()->with('error','Password Should be match');

            return redirect('add_user');
            
        }
        

    }

    public function login() {

        return view('pro/login'); // html of login

    }


    public function login_submit(Request $request) {

        $request->validate(['email' => 'required',
                            'password'  => 'required']);

        $data = $request->input();

    }


    public function profile(Request $request) {

        $email = session()->get('email');
        $user  = User::whereEmail($email)->first();

        return view('pro/profile', compact('user'));

        // // return view('profile');

    }


    public function logout(Request $request) {

       if (session()->has('email')) {

          session()->pull('email');

         }

         return redirect('login');

    }

    public function edit() {

        $email = session()->get('email');
        $user = User::whereEmail($email)->first();
       
        return view('pro/update', compact('user'));

    }

    public function update_submit(Request $request) {

        $email = session()->get('email');
        $request->validate(['first_name'       => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'last_name'        => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'username'         => 'required | max:10|regex:/^[\pL\s\-]+$/u',
                            'phone_number'     => 'required | max:10']);

        $first_name       = $request->input('first_name');
        $last_name        = $request->input('last_name');
        $username         = $request->input('username');
        $phone_number     = $request->input('phone_number');
        $password         = $request->input('password');
        $confirm_password = $request->input('confirm_password');


        DB::insert("UPDATE user SET first_name = '$first_name', last_name = '$last_name', username = '$username', email = '$email', phone_number = '$phone_number' WHERE email = '$email'");

        // return redirect('edit-profile');
        return redirect('profile');

    }


    public function listing(){

        $users = DB::table('user')->get();

        return view('pro/listing', compact('users'));
        // return view('listing');
    }
    
    public function delete($id){

       DB::insert("DELETE FROM user WHERE id=$id");

        return redirect('listing');
    }



    public function update_listing($id){

        $user = User::whereId($id)->first();

        return view('pro/update', compact('user'));
    }

    public function submit_update(Request $request){

        $request->validate(['first_name'  => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'last_name'   => 'required | max:10 |regex:/^[\pL\s\-]+$/u',
                            'username'    => 'required | max:10|regex:/^[\pL\s\-]+$/u',
                            'phone_number'=> 'required | max:10']);

            
            $id               = $request->input('id');
            $first_name       = $request->input('first_name');
            $first_name       = $request->input('first_name');
            $last_name        = $request->input('last_name');
            $username         = $request->input('username');
            $phone_number     = $request->input('phone_number');
            $password         = $request->input('password');
            $confirm_password = $request->input('confirm_password');

            DB::insert("UPDATE user SET first_name = '$first_name', last_name = '$last_name', username = '$username', phone_number = '$phone_number',  WHERE id = '$id'");

               return redirect('user_listing');

            }

////////////////////////////////// FOR ADMIN ///////////////////////////////////


    public function dashborad() {

        return view('admin/dashborad');
    }

    public function admin_login() {

        return view('admin/login');
        
    }

    public function add_user () {

        return view('admin/add_user');

    }

    public function user_listing () {


        $users = DB::table('user')->get();

        return view('admin/user_listing', compact('users'));

    }

    public function user_edit($id){

        $user = User::whereId($id)->first();

        return view('admin/user_edit', compact('user'));
    }


} //end class