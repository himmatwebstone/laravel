<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\UserAuth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index'])->name('index'); // Render html signup page to root
Route::post('submit', [UserController::class, 'submit']); //submit of register

Route::get('login', [UserController::class, 'login']); // Render html login

Route::post('user', [UserAuth::class, 'userLogin'])->name('user'); // call controller UserAuth

Route::get('profile', [UserController::class, 'profile'])->name('profile');// Render html of update page and data

Route::get('logout', [UserController::class, 'logout']); // Logout

Route::get('edit-profile', [UserController::class, 'edit'])->name('edit-profile');// Render html of update page and data

Route::post('update_submit', [UserController::class, 'update_submit']); // Submit data of update

Route::get('listing', [UserController::class, 'listing'])->name('listing'); // Render listing data

Route::get('delete/{id}', [UserController::class, 'delete'])->name('delete');// delete data for listing

Route::get('update_listing/{id}',[UserController::class, 'update_listing'])->name('update_listing');// render html with data for listing

Route::post('submit_update', [UserController::class, 'submit_update'])->name('submit_update');//submit data of update
Route::get('/admin', [UserController::class, 'dashborad'])->name('/admin');// admin template

////////////////////////////////// FOR ADMIN ///////////////////////////////////


Route::get('add_user', [UserController::class, 'add_user'])->name('add_user');// admin

Route::get('user_listing', [UserController::class, 'user_listing'])->name('user_listing');// admin

Route::get('user_edit/{id}', [UserController::class, 'user_edit'])->name('user_edit');// admin

Route::get('admin_login', [UserController::class, 'admin_login'])->name('admin_login');// admin

// Route::get('submit_admin_login', [UserController::class, 'submit_admin_login'])->name('submit_admin_login');// submit admin login admin